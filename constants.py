'''
Constants to be imported by Space Fighto
'''

'''
Copyright (C) 2012 Brendan Scott

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/

Version info:
1.0beta 22 November 2012

'''



import math
import pygameVec2d as VL
#from SFsettings import *

SCREEN_WIDTH=1366-10 # 1910
SCREEN_HEIGHT=768-10 # 1000


SCREEN_CENTRE = VL.Vec2d(SCREEN_WIDTH/2, SCREEN_HEIGHT/2)
DEBUG_VERBOSITY =1
WHITE = (255, 255, 255)
PINK = (252, 15, 192)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

FRICTION_FACTOR=0.99

FPS = 60

FP_ERROR = 1e-6


