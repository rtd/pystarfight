1.0 beta  Initial Release

Playable, basic 2d vector space game

F1 for settings (use p to unpause after setting settings), 
p for pause/unpause
Triangle controls: a,w,d, TAB to fire
Hex controls: up,L,R arrows, ctrl to fire 
h to show help message
s for bullet time


Screen size is hard coded atm 
To change screen size edit constants.py file:
SCREEN_WIDTH= <your width here> 
SCREEN_HEIGHT=<your height here>

Fighto!

r5:
replaced pygame.font with BasicVectorFont (pyinstaller should work now)
